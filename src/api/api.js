import axios from "axios";
import router from "@/router";

const axiosInstance = axios.create({
  baseURL: "http://chatterly-back.staj.bid",
});

// Перехватчик запроса, проверяет есть ли токен
axiosInstance.interceptors.request.use((config) => {
  const token = localStorage.getItem("access_token");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const originalRequest = error.config;
    if (error.response.status === 403 || error.response.status === 401) {
      const refreshToken = localStorage.getItem("refresh_token");
      if (!refreshToken) {
        router.push({ name: "forma" });
        localStorage.removeItem("access_token");
        return Promise.reject(error);
      }
      return axiosInstance
        .post("/auth/refresh", {
          refresh_token: refreshToken,
        })
        .then((response) => {
          localStorage.setItem("access_token", response.data.access_token);
          originalRequest.headers.Authorization = `Bearer ${response.data.access_token}`;
          return axiosInstance(originalRequest);
        })
        .catch((error) => {
          router.push({ name: "forma" });
          localStorage.removeItem("access_token");
          localStorage.removeItem("refresh_token");
          return Promise.reject(error);
        });
    } else {
      return Promise.reject(error);
    }
  }
);

export default axiosInstance;
